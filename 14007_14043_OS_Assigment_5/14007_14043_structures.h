#ifndef A_14007_14043_STRUCTURES_H
#define A_14007_14043_STRUCTURES_H
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
typedef struct Inode_t{
    int filesize;
    time_t mtime;        // modification time
    time_t ctime;            // create time
    time_t time;              // access time
    int data_ptr;             // block no

}Inode_t;

typedef struct super_t{
	int root; // index of inode on file  0th position on file is location of root;
	int I_bitmap;   //position of start of inode bitmap on file
	int D_bitmap;   // position of start of data_bitmap on file
	int inode;     // position of start of inode blocks on file
	int datablk;    //position of start of  data blocks on file
	
	int InodesPerBlock;                // no of inodes in each block
	int nInodes;               // no of total inodes = 8 * no of inode blocks
	int nblocks;                              // total no of blocks in fs
	int nI_blocks, nD_blocks;                 // no of inode blocks & no of data blocks
	
	int blocksize;                     // block size
	int inode_size ;                    //inode size
	int n_DPtr;                          //no of data blck pointers per inode
	int nbytes;
}super_t;

super_t super;
#endif
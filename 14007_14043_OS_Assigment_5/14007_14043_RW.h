#ifndef A_14007_14043_RW_H
#define A_14007_14043_RW_H
#include "14007_14043_structures.h"

int readData( int disk, int blockNum, void* block){
	int seek = super.datablk + (blockNum * super.blocksize);
	lseek(disk,seek, SEEK_SET);                      // seek the block to read data from
	char c ;
	int i = 1;
	read(disk, &c, sizeof(char));
	char *a = (char*) malloc(sizeof(char));
	while( c != '\0'){                                  // if end of file reached
		strncpy(a, &c, 1); 
		strcat((char*) block, a);            //concatenate read byte to block
		read(disk, &c, sizeof(char));     //read byte by byte
		i++;
	}
	return i;
}

int readInode( int disk, int inodeNum, void* block){
	int seek = super.inode + (inodeNum * super.inode_size);
	lseek(disk,seek, SEEK_SET);                           //seek the inode to read meta data from
	read(disk, block, sizeof(Inode_t));
	((Inode_t*) block)->time = time(NULL);
	writeInode(disk, inodeNum, block);
}
int writeInode(int disk, int inodeNum, void* block){
	int seek = super.inode + (inodeNum * super.inode_size);	
	lseek(disk,seek, SEEK_SET);                          //seek the inode 
	write(disk, block, sizeof(Inode_t));                 //write the whole inode to disk
}
void markBitmap(int disk, int16_t i, int start)
{
	int seek = start + (i/8);                             // ith bit among 256 * 8 bits store
	lseek(disk,seek, SEEK_SET);

	char c;
	i = i % 8;
	read(disk, &c, sizeof(char));                     //read the current byte
	lseek(disk,seek, SEEK_SET);
	c = c | (0b10000000 >> i);                       // mark one on bitmap whose datablock/inode has been utilised
	write(disk, &c, sizeof(char));
}

int findEmptyBlock(int disk, int seek)
{
	lseek(disk, seek, SEEK_SET);
	char c;
	int i = 0;
	if (super.nD_blocks < 8)
	{
		read(disk, &c, sizeof(char) );
		while(i < super.nD_blocks)
		{
			if((c & (0x80 >> i) )== 0x00) break;          //if the ith block is not occupied
			i++;
		}
		if(i  == super.nD_blocks) return -1;
		return i;
	}
	i = 0;
	int nblks = (super.nblocks % 8 == 0)?super.nblocks/8 : (super.nblocks /8 +1);
	while(i < nblks)
	{
		read(disk, &c, sizeof(char) );
		if(c != 0xFF) break;                               //if all the blocks from current byte are not occupied
		i++;

	}
	if(i == nblks) return -1;
	int j = 0 ;
	while(j < 8){
		if(((0x80 >> j) & c)  == 0x00)		 //from the current byte figure out the position which is free 
		break;
		j++;
	}
	return i * 8 + j;
}

void unmarkBitmap(int disk, int16_t i, int start)
{
	int seek = start + (i/8);                             // ith bit among 256 * 8 bits store
	lseek(disk,seek, SEEK_SET);

	char c;
	i = i % 8;
	read(disk, &c, sizeof(char));
	lseek(disk,seek, SEEK_SET);
	c = c & ~(0b10000000 >> i);                    // make that block as available
	write(disk, &c, sizeof(char));
}

int writeData(int disk, int blockNum, void* block){
	int seek = super.datablk + (blockNum * super.blocksize);
	markBitmap(disk, blockNum, super.D_bitmap);                         /// mark that this block is now used

	int len = strlen((char*) block);
	if(strlen(block) > super.blocksize)
	{
		int dataPos;
		if((dataPos = findEmptyBlock(disk, super.D_bitmap) )== -1) return -1;
		if(dataPos != blockNum + 1) return -1;
		lseek(disk,seek, SEEK_SET);
		// write(disk, block, super.blocksize);
		int l, l1;
		l = write(disk, block, super.blocksize);
		if((l1 = writeData(disk,dataPos, block + super.blocksize )) == -1){
			unmarkBitmap(disk, blockNum, super.D_bitmap);            //if the entire data couldnt be written unmark those blocks which got marked for the current write
			return -1;	
		} 
		return ( l + l1);

	}
	lseek(disk,seek, SEEK_SET);
	return write(disk, block, len+1);                        //write  to file
}
#endif


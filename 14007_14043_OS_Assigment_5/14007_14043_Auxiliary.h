#ifndef A_14007_14043_AUXILIARY_H
#define A_14007_14043_AUXILIARY_H 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int* toBinary(unsigned char ch)               //convert byte to binary 
{

	int a = ch, i = 0,j;
	int b[8] = {0};
	while(a != 0)
	{
		b[i] = a % 2;
		a/=2;
		i++; 	
	}
	int *c = (int*)malloc(8*sizeof(int));
	for(i = 0, j = 7; i < 8; i++, j--)
		c[j] = b[i];
	return c;
}

void printBinary(unsigned char c)          // print binary
{
	int *b = toBinary(c);
	int i;
	for(i = 0 ;i < 8; i++)
		printf("%d",b[i] );
}

int parser(char a[],char *c[])                    //parse the given array to read files stored at root data block
{
	if(!strlen(a))
		return 0;
	char *b=strtok(a,"%%&");
	int i=0,count=0;
	c[i] = (char*)malloc(sizeof(char)*128);
	strcpy(c[i++],b);
	while(b)
	{
		if(!(count&1))
			printf("%s\n",b);
		c[i] = (char*)malloc(sizeof(char)*128);
		strcpy(c[i++],b);
		count++;
		b=strtok(NULL,"%%&");
	}
	return 1;
}

int getInode(char a[],char t[])                                             // get the inode where the given file name's meta data is stored
{
	if(strlen(a) == 0) return -1;
	
	char *b=strtok(a,"%%&");
	
	int i=0,j;
	char *c[100];
	for(i=0;i<100;i++)
	    c[i]=(char*)malloc(sizeof(char)*100);
	i=0;
	strcpy(c[i++],b);
	while(b)
	{
		strcpy(c[i++],b);
		b=strtok(NULL,"%%&");
	}
	int sum=-1;
	char *temp;
	for(j = 1; j < i; j++)
	    if(strcmp(c[j], t) == 0)
	    	sum = c[j+1][0];
	return sum;
}
#endif
#include <stdio.h>
#include <stdlib.h>
int searchChar(char *str , char c)
{
	int i = 0;
	while(*str != '\0')
	{
		if(*str == c) return ++i;    // give position of next char
		str++;
		i++;
	}
	return -1;
}

int* toBinary(char ch)
{

	int a = ch, i = 0,j;
	int b[8] = {0};
	while(a != 0)
	{
		b[i] = a % 2;
		a/=2;
		i++; 	
	}
	int *c = (int*)malloc(8*sizeof(int));
	for(i = 0, j = 7; i < 8; i++, j--)
		c[j] = b[i];
	return c;
}

void printBinary(char c)
{
	int *b = toBinary(c);
	int i;
	for(i = 0 ;i < 8; i++)
		printf("%d",b[i] );
	// b[8]= '\0';
	// return b;
}
int main(void) {
	 printBinary(6);
	return 0;
}

#ifndef A_14007_14043_UI_RW_H
#define A_14007_14043_UI_RW_H
#endif
#include "14007_14043_Auxiliary.h"
#include "14007_14043_RW.h"
#include <stdlib.h>

void genFileName(char* file, char *filename, char inodePos){             //gen the file name format used to store data on root dat block
	char *a = (char * )malloc(sizeof(char));
	strcpy(file, "&");
	strcat(file, filename);
	strcpy(a, "%");
	strcat(file, a);
	char *b = (char*)malloc(sizeof(char));
	strncpy(b, &inodePos, 1);                                                      //format a.txt%2&b.txt%3
	strcat(file, b);	
}
int writeFile(int disk, char* filename, void* block){
	
	char inodePos, dataPos;
	if((inodePos = findEmptyBlock(disk, super.I_bitmap) )== -1) return -1;                   //find an empty/available inode block
	if((dataPos = findEmptyBlock(disk, super.D_bitmap)) == -1) return -1;                  //find an empty/available data block
	Inode_t *i = (Inode_t * )malloc(sizeof(Inode_t));
	char *d = (char*)malloc(super.blocksize);
	readInode(disk, super.root, (void*)i);                                                                    //read root inode 
	readData(disk, ((Inode_t*)i)->data_ptr, (void*)d );                                           //read data block of root
	char *cpy = (char *) malloc(strlen(filename) + 1); 
	strcpy(cpy, d);	
	if(getInode(cpy, filename) != -1) return -2;                                              //see if file already exists on file
	char *file  = (char*) malloc((sizeof(char))*128);
	genFileName(file, filename, inodePos);                                                     //generate desired file name to append to data block
	strcat(d , file);
	int l =  writeData(disk, dataPos, block);                            // write data to file and check if successfule

	if(l == -1) return l;
	writeData(disk, i->data_ptr, (void*) d);                                                  // if successful append the new file name 
	markBitmap(disk, (int)inodePos, super.I_bitmap);
	Inode_t *inode_data = (Inode_t*)malloc(sizeof(Inode_t));                                   //declare inode pointer
	inode_data->filesize = strlen(block);
	inode_data->ctime = time(NULL);
	inode_data->time = time(NULL);
	inode_data->data_ptr = dataPos;                                 // 0th data block
	writeInode(disk, inodePos, (void*)inode_data);                //write inode data on the inode block 
	return l;
}
int readFile(int disk, char* filename, void* block){
	Inode_t * inode = (Inode_t*)malloc(sizeof(Inode_t));
	readInode(disk, super.root, (void*)inode);                           //read super inode
	char *d = (char*)malloc(sizeof(char) * super.blocksize); 
	readData(disk,inode->data_ptr, (void*)d);                          //read files on disk
	int node = getInode(d,filename);                                         //get the inode of the desired file
	if(node == -1) return -1;                                            //if file doesnt exist

	readInode(disk,node, (void*)inode);                           //read the inode of the desiredffile
	return readData(disk, inode->data_ptr, block);            // read data 
}

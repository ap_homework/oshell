#include "14007_14043_CreateSFS.h"
#include "14007_14043_UI_RW.h"

void print_FileList(int fileSystemId){
	Inode_t * inode_data = (Inode_t*)malloc (sizeof(Inode_t));
	readInode(fileSystemId,super.root,(void*)inode_data);                   //read root inode
	char * data = (char*)malloc(sizeof(char)*super.blocksize); 
	readData(fileSystemId,inode_data->data_ptr, (void*)data);            // read data on block pointed by root inode
	char *files[super.blocksize];
	if(!parser((char*)data,files)) 
    		printf("No files on disk");                                                 // parse the files stored on block
	printf("\n");
}
void print_dataBitmaps(int fileSystemId){
	char * dataBitmap;
	int i = 0 ;
	lseek(fileSystemId, super.D_bitmap, SEEK_SET);
	char c;
	int nblks = (super.nD_blocks % 8 == 0)?super.nD_blocks/8 : (super.nD_blocks /8 +1);             //find no of bytesneeded to print the entire useful bitmap block
	printf("No of Data Blocks : %d \n", super.nD_blocks);
	while(i < nblks)
	{
		read(fileSystemId, &c, sizeof(char));                          //read byte 
		printBinary(c);                                    // convert current byte to binary and print
		i++;
	}
	printf("\n");
}
void print_inodeBitmaps(int fileSystemId){
	char * inodeBitmap;
	int i = 0 ;
	lseek(fileSystemId, super.I_bitmap, SEEK_SET);
	unsigned char c;
            int nblks = (super.nInodes % 8 == 0)?super.nInodes/8 : (super.nInodes /8 +1);                    //find no of bytes to read to print the useful bitmap block
	// int nblks = (super.nI_blocks % 8 == 0)?super.nI_blocks/8 : (super.nI_blocks /8 +1);                    //find no of bytes to read to print the useful bitmap block
	printf("No of Inodes : %d\n", super.nInodes );
	while(i < nblks)
	{
		read(fileSystemId, &c, sizeof(char));
		printBinary(c);
		i++;
	}
	printf("\n");
}
int main()
{
    int disk,x,c, flag = 0;
    while(1){
    	printf("1) Create SFS\n2) Write data to file\n3) Read data from file\n4) List all files\n5) List all inodes\n6) List all data blocks\n7) Exit\n" );
    	printf("Enter option (1-7) : ");
    	scanf("%d",&x);
	char a[1024],b[100000];   
	char *D = (char*)malloc(sizeof(char)*100*super.blocksize); 	
	int size;
    	switch(x)
    	{
    		case 1:
    		printf("Enter disk name to be created : ");
    		scanf("%s",a);
    		printf("Enter disk size (in bytes) : ");
    		scanf("%d",&size);
    		if((disk = createSFS(a,size)) == -1)
    		{
    			printf("Enter valid disk size.\n");
    		}
                      else flag =1;
    		break;
    		case 2:
    		if(!flag ) {
                                printf("First create a disk\n");
                                break;
                       }
                      printf("Enter file name to be written to : ");
    		scanf("%s",a);
    		printf("Enter content to be written to file : ");
    		scanf("%s",b);
    		// fgets(b, 100000, stdin);
                      int ret = writeFile(disk,a,b); 
    		if( ret == -1)
    			printf("File size too large.\n");
    		else if( ret == -2)
    			printf("File already exists on disk.\n");
    		else
    		{
    			printf("Number of bytes written to %s : %d\n",a,ret);
    			printf("File successfully written to disk.\n");
    		}
    		break;
    		case 3:
    		if(!flag ) {
                                printf("First create a disk\n");
                                break;
                       }
                       printf("Enter file name to be read from : ");
    		scanf("%s",a);
    		if((size=readFile(disk, a, D)) == -1)
    			printf("%s doesn't exist on disk.\n",a);    
    		else
    		{
    			printf("Number of bytes read from %s : %d\n",a,size);
    			printf("The following data was read from the file : %s\n",(char*)(D));
    		}
    		break;
    		case 4:
    		if(!flag ) {
                                printf("First create a disk\n");
                                break;
                       }
                      printf("The following are the files on the current disk : \n");
    		print_FileList(disk);
    		break;
    		case 5:
                       if(!flag ) {
                                printf("First create a disk\n");
                                break;
                       }
    		printf("The following is the inode bitmap on the current disk : \n");
    		print_inodeBitmaps(disk);
    		break;
    		case 6:
                       if(!flag ) {
                                printf("First create a disk\n");
                                break;
                       }
    		printf("The following is the data bitmap on the current disk : \n");
    		print_dataBitmaps(disk);
    		break;
    		case 7:
    		if(disk > 2)
                                close(disk);
    		return 0;
    		break;
    		default:
    		printf("Enter valid option.\n");
    		break;	
    	}
    }
    close(disk);
    return 0;
}
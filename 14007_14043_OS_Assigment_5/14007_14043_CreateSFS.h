
#include "14007_14043_RW.h"
void writeZeroToFile(int disk, int nbytes)
{
	int i = 0 ;
	char c = 0x00;
	while(i++ < nbytes)
		write(disk, &c, sizeof(char));
}

int createSFS( char* filename, int nbytes){
	

	int disk = open(filename, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IXUSR 	);                   // set size of file left else ... eof will arise :/
	if(disk == -1)
	{
	    printf("error opening file\n");
	    return -1;
	}
	// lseek(disk, nbytes*8 ,SEEK_SET);               // set the file size to nbytes
	writeZeroToFile(disk, nbytes);    						// write 0's (nbytes) to file
	super.blocksize = 256;
	if(nbytes < super.blocksize * 20) return -1;
	
	super.nbytes = nbytes;
	super.inode_size = sizeof(Inode_t);
	super.InodesPerBlock = super.blocksize / super.inode_size;                       //256/ 16
	super.n_DPtr = 1;
	super.nblocks = nbytes / super.blocksize;
	// super.nD_blocks = (super.nblocks - 3) / 2;                    // no of data blocks
	// super.nI_blocks = super.nD_blocks ;                      // no of  inode block

	super.nD_blocks = (super.nblocks - 3) / (super.InodesPerBlock+1);
	super.nI_blocks = super.nD_blocks / super.InodesPerBlock;
	super.nInodes = super.nD_blocks;

	if(super.nI_blocks == 0  || super.nD_blocks == 0) return -1;
	
	super.I_bitmap = super.blocksize;                                // 2nd Block
	super.D_bitmap = super.blocksize * 2;                                // 3rd Block
	super.inode = super.blocksize * 3;                                   //4th block , nI_blocks
	super.datablk = (super.inode + super.nI_blocks*256);     //768 + nI_blocks*256
	
	super.root = 0;                                       // 1st inode
	lseek(disk,0,SEEK_SET);
	write(disk, &super,sizeof(super));	
	Inode_t *inode_data = (Inode_t*)malloc(sizeof(Inode_t));                                   //declare inode pointer
	inode_data->filesize = super.nD_blocks * super. blocksize;
	inode_data->ctime = time(NULL);
	inode_data->mtime = time(NULL);
	inode_data->time = time(NULL);
	inode_data->data_ptr = 0;                                 // 0th data block
	writeInode(disk, super.root, (void*)inode_data);
	markBitmap(disk, super.root, super.I_bitmap);              // mark that the 0th inode is used up

	readInode(disk, super.root, (void*)inode_data);               // inode data should have full inode structure read
	
	char *data = (char *)malloc(sizeof(char) * super.blocksize ); //declare data pointer
	writeData(disk,inode_data->data_ptr, (void*)data);
	markBitmap(disk,inode_data->data_ptr,super.D_bitmap);                         // mark that the 1st inode blck is used for home
	return disk;
}